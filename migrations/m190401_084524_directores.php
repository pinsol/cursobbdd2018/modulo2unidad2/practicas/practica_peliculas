<?php

use yii\db\Migration;

/**
 * Class m190401_084524_directores
 */
class m190401_084524_directores extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("directores", [
            "id"=>$this->primaryKey(),
            "nombre"=>$this->string(50),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('directores');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190401_084524_directores cannot be reverted.\n";

        return false;
    }
    */
}
