<?php
    use yii\helpers\Html;
?>

<div class="jumbotron">
<h1><?= $model->titulo ?></h1>
</div>

<div class="row">
<?= Html::img('@web/imgs/' . $model->cartel, [
    'alt' => 'My logo',
    'width'=> 200,
    'class'=> 'img-responsive img-thumbnail'
    ])
?>
</div>

<div class="row">
<ul>
    <li>Descripción: <?= $model->descripcion ?></li>
    <li>Año: <?= $model->year ?></li>
    <li>Duración: <?= $model->duracion ?></li>
</ul>
</div>