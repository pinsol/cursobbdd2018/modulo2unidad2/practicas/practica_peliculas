<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);                  // PONER EN COMENTARIO CUANDO LA APP ESTE EN EXPLOTACION quita la barrita de estado
defined('YII_ENV') or define('YII_ENV', 'dev');                     // PONER EN COMENTARIO CUANDO LA APP ESTE EN EXPLOTACION - cambiar la pantalla de error en views/site/error.php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
